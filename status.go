package bugclose

/**
 * @Copyright (C) 2022

 * @Author: baoshuangh

 * @Date: 2022/11/14
**/

type UpdateType string

const (
	Reopen  UpdateType = "reopen"
	Fix     UpdateType = "fix"
	Approve UpdateType = "approve"
	Close   UpdateType = "close"
)
