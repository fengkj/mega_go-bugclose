package bugclose

import (
	"encoding/json"
	"io"
	"net/http"

	"github.com/tidwall/gjson"
)

type IssuesResponse struct {
	Root []struct {
		Active           bool          `json:"active"`
		AllowActions     []string      `json:"allowActions"`
		AssignToId       int           `json:"assignToId"`
		AssignedById     int           `json:"assignedById"`
		AssignedTime     int64         `json:"assignedTime"`
		BugType          string        `json:"bugType"`
		Confirmed        bool          `json:"confirmed"`
		CreatedById      int           `json:"createdById"`
		CreatedIP        string        `json:"createdIP"`
		CreatedTime      int64         `json:"createdTime"`
		CustomerId       int           `json:"customerId"`
		Environment      string        `json:"environment"`
		FixedById        int           `json:"fixedById"`
		Id               int           `json:"id"`
		LastAssignedById int           `json:"lastAssignedById"`
		LastAssignedTime int64         `json:"lastAssignedTime,omitempty"`
		ModifiedById     int           `json:"modifiedById"`
		ModifiedTime     int64         `json:"modifiedTime"`
		Module           string        `json:"module"`
		NoPass           bool          `json:"noPass"`
		Number           int           `json:"number"`
		ParentId         int           `json:"parentId"`
		Percent          int           `json:"percent"`
		PlanVersion      string        `json:"planVersion"`
		Priority         string        `json:"priority"`
		ProductId        int           `json:"productId"`
		ProjectId        int           `json:"projectId"`
		Reopened         bool          `json:"reopened"`
		Reviewed         bool          `json:"reviewed"`
		SprintId         int           `json:"sprintId"`
		State            string        `json:"state"`
		SubTypeId        int           `json:"subTypeId"`
		TagIds           []interface{} `json:"tagIds"`
		TestCaseId       int           `json:"testCaseId"`
		TestTaskId       int           `json:"testTaskId"`
		Title            string        `json:"title"`
		Treatment        string        `json:"treatment"`
		Value            int           `json:"value"`
		VerifierId       int           `json:"verifierId"`
		Version          string        `json:"version"`
		Workload         int           `json:"workload"`
		FixedTime        int64         `json:"fixedTime,omitempty"`
	} `json:"root"`
	Success bool `json:"success"`
}

type Attachments struct {
	CreatedIP        string `json:"createdIP"`
	Id               int    `json:"id"`
	OriginalFileName string `json:"originalFileName"`
	ProjectId        int    `json:"projectId"`
	UploadTime       int64  `json:"uploadTime"`
	Url              string `json:"url"`
	UserId           int    `json:"userId"`
}

type CustomValues struct {
}

type Detail struct {
	Attachments    []Attachments `json:"attachments"`
	Images         []Attachments `json:"images"`
	CustomValues   CustomValues  `json:"customValues"`
	Description    string        `json:"description"`
	Id             int           `json:"id"`
	Markdown       bool          `json:"markdown"`
	ParticipantIds []int         `json:"participantIds"`
}

type IssueDetails struct {
	Root struct {
		Active       bool     `json:"active"`
		AllowActions []string `json:"allowActions"`
		AssignToId   int      `json:"assignToId"`
		AssignedById int      `json:"assignedById"`
		AssignedTime int64    `json:"assignedTime"`
		BugType      string   `json:"bugType"`
		Confirmed    bool     `json:"confirmed"`
		CreatedById  int      `json:"createdById"`
		CreatedIP    string   `json:"createdIP"`
		CreatedTime  int64    `json:"createdTime"`
		CustomerId   int      `json:"customerId"`
		Detail       Detail   `json:"detail"`
		Environment  string   `json:"environment"`
		FixedById    int      `json:"fixedById"`
		Histories    []struct {
			Action       string   `json:"action"`
			AllowActions []string `json:"allowActions"`
			AssignToId   int      `json:"assignToId"`
			BugId        int      `json:"bugId"`
			BugType      string   `json:"bugType"`
			CreatedIP    string   `json:"createdIP"`
			Detail       struct {
				BugId        int `json:"bugId"`
				CustomValues struct {
					Field1 string `json:"58"`
					Field2 string `json:"59"`
					Field3 string `json:"60"`
					Field4 string `json:"61"`
				} `json:"customValues"`
				Description string `json:"description"`
				Id          int    `json:"id"`
				Markdown    bool   `json:"markdown"`
			} `json:"detail,omitempty"`
			Environment  string        `json:"environment,omitempty"`
			Id           int           `json:"id"`
			Markdown     bool          `json:"markdown"`
			ModifiedById int           `json:"modifiedById"`
			ModifiedTime int64         `json:"modifiedTime"`
			Module       string        `json:"module,omitempty"`
			Percent      int           `json:"percent"`
			PlanVersion  string        `json:"planVersion,omitempty"`
			Priority     string        `json:"priority"`
			ProductId    int           `json:"productId"`
			SprintId     int           `json:"sprintId"`
			State        string        `json:"state"`
			SubTypeId    int           `json:"subTypeId"`
			TagIds       []int         `json:"tagIds,omitempty"`
			Title        string        `json:"title,omitempty"`
			Treatment    string        `json:"treatment"`
			Value        int           `json:"value"`
			Version      string        `json:"version,omitempty"`
			Workload     int           `json:"workload"`
			Attachments  []Attachments `json:"attachments,omitempty"`
			Comments     string        `json:"comments,omitempty"`
		} `json:"histories"`
		Id               int           `json:"id"`
		LastAssignedById int           `json:"lastAssignedById"`
		LastAssignedTime int64         `json:"lastAssignedTime"`
		ModifiedById     int           `json:"modifiedById"`
		ModifiedTime     int64         `json:"modifiedTime"`
		Module           string        `json:"module"`
		NoPass           bool          `json:"noPass"`
		Number           int           `json:"number"`
		ParentId         int           `json:"parentId"`
		Percent          int           `json:"percent"`
		PostTasks        []interface{} `json:"postTasks"`
		PreTasks         []interface{} `json:"preTasks"`
		Priority         string        `json:"priority"`
		ProductId        int           `json:"productId"`
		ProjectId        int           `json:"projectId"`
		RelatedBugs      []struct {
			Active           bool   `json:"active"`
			AssignToId       int    `json:"assignToId"`
			AssignedById     int    `json:"assignedById"`
			AssignedTime     int64  `json:"assignedTime"`
			BugType          string `json:"bugType"`
			Confirmed        bool   `json:"confirmed"`
			CreatedById      int    `json:"createdById"`
			CreatedIP        string `json:"createdIP"`
			CreatedTime      int64  `json:"createdTime"`
			CustomerId       int    `json:"customerId"`
			Environment      string `json:"environment"`
			FixedById        int    `json:"fixedById"`
			Id               int    `json:"id"`
			LastAssignedById int    `json:"lastAssignedById"`
			LastAssignedTime int64  `json:"lastAssignedTime"`
			ModifiedById     int    `json:"modifiedById"`
			ModifiedTime     int64  `json:"modifiedTime"`
			Module           string `json:"module"`
			NoPass           bool   `json:"noPass"`
			Number           int    `json:"number"`
			ParentId         int    `json:"parentId"`
			Percent          int    `json:"percent"`
			PlanVersion      string `json:"planVersion"`
			Priority         string `json:"priority"`
			Product          struct {
				BugAssignToId     int    `json:"bugAssignToId"`
				BugVerifierId     int    `json:"bugVerifierId"`
				Closed            bool   `json:"closed"`
				DefaultAssignToId int    `json:"defaultAssignToId"`
				Id                int    `json:"id"`
				Name              string `json:"name"`
				ProjectId         int    `json:"projectId"`
				RequireAssignToId int    `json:"requireAssignToId"`
				RequireVerifierId int    `json:"requireVerifierId"`
				TaskAssignToId    int    `json:"taskAssignToId"`
				TaskVerifierId    int    `json:"taskVerifierId"`
			} `json:"product"`
			ProductId int `json:"productId"`
			Project   struct {
				AllowConfig             bool   `json:"allowConfig"`
				BugInfoEnabled          bool   `json:"bugInfoEnabled"`
				Closed                  bool   `json:"closed"`
				ConfirmEnabled          bool   `json:"confirmEnabled"`
				CreatedTime             int64  `json:"createdTime"`
				CustomEnabled           bool   `json:"customEnabled"`
				CustomerEnabled         bool   `json:"customerEnabled"`
				DisplayImageNameEnabled bool   `json:"displayImageNameEnabled"`
				DisplayUserInfoEnabled  bool   `json:"displayUserInfoEnabled"`
				DueDate                 int64  `json:"dueDate"`
				EnvEnabled              bool   `json:"envEnabled"`
				EvaEnabled              bool   `json:"evaEnabled"`
				FocusEnabled            bool   `json:"focusEnabled"`
				Id                      int    `json:"id"`
				ModuleEnabled           bool   `json:"moduleEnabled"`
				Name                    string `json:"name"`
				OwnerExcluded           bool   `json:"ownerExcluded"`
				OwnerId                 int    `json:"ownerId"`
				PartnerEnabled          bool   `json:"partnerEnabled"`
				PlanDate                int64  `json:"planDate"`
				Professional            bool   `json:"professional"`
				ReadOnly                bool   `json:"readOnly"`
				ReviewEnabled           bool   `json:"reviewEnabled"`
				SprintEnabled           bool   `json:"sprintEnabled"`
				State                   string `json:"state"`
				SubTaskEnabled          bool   `json:"subTaskEnabled"`
				TagEnabled              bool   `json:"tagEnabled"`
				TestCaseEnabled         bool   `json:"testCaseEnabled"`
				UserNumber              int    `json:"userNumber"`
				VersionEnabled          bool   `json:"versionEnabled"`
				WeekBeginMonday         bool   `json:"weekBeginMonday"`
			} `json:"project"`
			ProjectId    int    `json:"projectId"`
			Reopened     bool   `json:"reopened"`
			Reviewed     bool   `json:"reviewed"`
			SprintId     int    `json:"sprintId"`
			State        string `json:"state"`
			SubTypeId    int    `json:"subTypeId"`
			TagIds       []int  `json:"tagIds"`
			TestCaseId   int    `json:"testCaseId"`
			TestTaskId   int    `json:"testTaskId"`
			Title        string `json:"title"`
			Treatment    string `json:"treatment"`
			Value        int    `json:"value"`
			VerifiedTime int64  `json:"verifiedTime"`
			VerifierId   int    `json:"verifierId"`
			Version      string `json:"version"`
			Workload     int    `json:"workload"`
		} `json:"relatedBugs"`
		Reopened   bool          `json:"reopened"`
		Reviewed   bool          `json:"reviewed"`
		SprintId   int           `json:"sprintId"`
		State      string        `json:"state"`
		SubBugs    []interface{} `json:"subBugs"`
		SubTypeId  int           `json:"subTypeId"`
		TagIds     []int         `json:"tagIds"`
		TestCaseId int           `json:"testCaseId"`
		TestTaskId int           `json:"testTaskId"`
		Title      string        `json:"title"`
		Treatment  string        `json:"treatment"`
		Value      int           `json:"value"`
		VerifierId int           `json:"verifierId"`
		Version    string        `json:"version"`
		Workload   int           `json:"workload"`
	} `json:"root"`
	Success bool `json:"success"`
}

type ModifyAndCreateRequest struct {
	Token         string `json:"token"`
	ProjectId     string `json:"projectId"`
	Id            string `json:"id"`
	Title         string `json:"title"`
	ImageIds      string `json:"imageIds"`
	BugType       string `json:"bugType"`
	AttachmentIds string `json:"attachmentIds"`
	Priority      string `json:"priority"`
	Version       string `json:"version"`
	Environment   string `json:"environment"`
	Description   string `json:"description"`
	ProductId     string `json:"productId"`
	AssignToId    string `json:"assignToId"`
	PlanVersion   string `json:"planVersion"`
	Module        string `json:"module"`
	CustomValues  string `json:"customValues"`
	Comments      string `json:"comments"`
}

type UpdateStatusRequest struct {
	Token      string `json:"token"`
	UpdateType UpdateType
	//ReopenStatus struct {
	ProjectId     string `json:"projectId"`
	Id            string `json:"id"`
	Comments      string `json:"comments"`
	ImageIds      string `json:"imageIds"`
	AttachmentIds string `json:"attachmentIds"`
	AssignToId    string `json:"assignToId"`
	PlanDate      string `json:"planDate"`
	DueDate       string `json:"dueDate"`
	CustomValues  string `json:"customValues"`
	Priority      string `json:"priority"`
	Environment   string `json:"environment"`
	//}
}

type AddCommentRequest struct {
	Token     string `json:"token"`
	ProjectId string `json:"projectId"`
	Id        string `json:"id"`
	Comments  string `json:"comments"`
}

type GetAllIssueRequest struct {
	Token      string `json:"token"`
	ProjectId  string `json:"projectId"`
	MaxResults string `json:"maxResults"`
}

type GetIssueDetailsRequest struct {
	Token     string `json:"token"`
	ProjectId string `json:"projectId"`
	Id        string `json:"id"`
	Number    string `json:"number"`
}

type CommentsResponse struct {
	Success bool `json:"success"`
}

type CreateIssueResponse struct {
	Root int `json:"root"`
}

type ModifyIssueResponse struct {
	Success bool `json:"success"`
}

type IssueService struct {
	client *Client
}

type StatusUpdateResponse struct {
	Success bool `json:"success"`
}

func (i *IssueService) GetAllIssue(r *GetAllIssueRequest) (string, IssuesResponse, error) {
	var is IssuesResponse
	endPoint := "bug/query"
	httpUrl := i.client.baseURL + endPoint
	body, writer, _ := i.client.marshalMap(r)
	req, _ := http.NewRequest(http.MethodPost, httpUrl, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := http.DefaultClient.Do(req)
	data, _ := io.ReadAll(res.Body)
	if err != nil {
		return string(data), is, err
	} else {
		err := json.Unmarshal(data, &is)
		if err != nil {
			return "", is, err
		}
	}
	return "", is, nil
}

func (i *IssueService) GetIssueDetails(r *GetIssueDetailsRequest) (string, IssueDetails, error) {
	var ids IssueDetails
	endPoint := "bug/get"
	httpUrl := i.client.baseURL + endPoint
	body, writer, _ := i.client.marshalMap(r)
	req, _ := http.NewRequest(http.MethodPost, httpUrl, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := http.DefaultClient.Do(req)
	data, _ := io.ReadAll(res.Body)
	if err != nil {
		return string(data), ids, err
	} else {
		err := json.Unmarshal(data, &ids)
		if err != nil {
			return "", ids, err
		}
	}
	return string(data), ids, nil
}

func (i *IssueService) AddComments(r *AddCommentRequest) (string, CommentsResponse, error) {
	var cms CommentsResponse
	endPoint := "bug/comment"
	httpUrl := i.client.baseURL + endPoint
	body, writer, _ := i.client.marshalMap(r)
	req, _ := http.NewRequest(http.MethodPost, httpUrl, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := http.DefaultClient.Do(req)
	data, _ := io.ReadAll(res.Body)
	if err != nil {
		return string(data), cms, err
	} else {
		err := json.Unmarshal(data, &cms)
		if err != nil {
			return string(data), cms, err
		}
	}
	return string(data), cms, nil
}

func (i *IssueService) CreateIssue(r *ModifyAndCreateRequest) (string, CreateIssueResponse, error) {
	var is CreateIssueResponse
	endPoint := "bug/add"
	httpUrl := i.client.baseURL + endPoint
	body, writer, _ := i.client.marshalMap(r)
	req, _ := http.NewRequest(http.MethodPost, httpUrl, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := http.DefaultClient.Do(req)
	data, _ := io.ReadAll(res.Body)
	if err != nil {
		return string(data), is, err
	} else {
		err := json.Unmarshal(data, &is)
		if err != nil {
			return string(data), is, err
		}
	}
	return string(data), is, nil
}

func (i *IssueService) ModifyIssue(r *ModifyAndCreateRequest) (string, ModifyIssueResponse, error) {
	var mi ModifyIssueResponse
	endPoint := "bug/modify"
	httpUrl := i.client.baseURL + endPoint
	body, writer, _ := i.client.marshalMap(r)
	req, _ := http.NewRequest(http.MethodPost, httpUrl, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := http.DefaultClient.Do(req)
	data, _ := io.ReadAll(res.Body)
	if err != nil {
		return string(data), mi, err
	} else {
		err := json.Unmarshal(data, &mi)
		if err != nil {
			return string(data), mi, err
		}
	}
	return string(data), mi, nil
}

func (i *IssueService) UpdateStatus(r *UpdateStatusRequest) (string, StatusUpdateResponse, error) {
	var su StatusUpdateResponse
	endPoint := "bug/" + string(r.UpdateType)
	httpUrl := i.client.baseURL + endPoint
	body, writer, _ := i.client.marshalMap(r)
	req, _ := http.NewRequest(http.MethodPost, httpUrl, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := http.DefaultClient.Do(req)
	data, _ := io.ReadAll(res.Body)
	if err != nil {
		return string(data), su, err
	} else {
		err := json.Unmarshal(data, &su)
		if err != nil {
			return string(data), su, err
		}
	}
	return string(data), su, nil
}

type BugCountRes struct {
	Root    int  `json:"root"`
	Success bool `json:"success"`
}

type BugCountReq struct {
	Token     string `json:"token"`
	ProjectId string `json:"projectId"`
	Id        string `json:"id"`
}

func (i *IssueService) BugCount(r *BugCountReq) (string, BugCountRes, error) {
	var bc BugCountRes
	endPoint := "bug/count"
	httpUrl := i.client.baseURL + endPoint
	body, writer, _ := i.client.marshalMap(r)
	req, _ := http.NewRequest(http.MethodPost, httpUrl, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := http.DefaultClient.Do(req)
	data, _ := io.ReadAll(res.Body)
	if err != nil {
		return string(data), bc, err
	} else {
		err := json.Unmarshal(data, &bc)
		if err != nil {
			return string(data), bc, err
		}
	}
	return string(data), bc, nil
}

func (c *CustomValues) Value(details, key string) (string, error) {
	value := gjson.Get(details, "root.detail.customValues."+key)
	return value.String(), nil
}
