package bugclose

import (
	"bytes"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"strings"
)

/**
 * @Copyright (C) 2022

 * @Author: baoshuangh

 * @Date: 2022/11/12
**/

type Client struct {
	baseURL           string
	email             string
	password          string
	ProjectService    *ProjectService
	IssueService      *IssueService
	AttachmentService *AttachmentService
	UserService       *UserService
}

type TokenResponse struct {
	Token  string `json:"root"`
	Status bool   `json:"success"`
}

func SetAuth(url, email, password string) *Client {
	if !strings.HasSuffix(url, "/") {
		url += "/"
	}
	c := &Client{
		baseURL:  url,
		email:    email,
		password: password,
	}

	c.ProjectService = &ProjectService{c}
	c.IssueService = &IssueService{c}
	c.AttachmentService = &AttachmentService{c}
	c.UserService = &UserService{c}
	return c
}

func (c *Client) marshalMap(t interface{}) (*bytes.Buffer, *multipart.Writer, error) {
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	marshal, err := json.Marshal(&t)
	if err != nil {
		return nil, nil, err
	}
	var m map[string]interface{}
	_ = json.Unmarshal(marshal, &m)
	for k, v := range m {
		if v != nil {
			_ = writer.WriteField(k, v.(string))
		}
	}
	err = writer.Close()
	return body, writer, nil
}

func (c *Client) GetToken() (int, string, TokenResponse, error) {
	endPoint := "user/login"
	httpUrl := c.baseURL + endPoint
	var t TokenResponse
	formValues := url.Values{}
	formValues.Set("email", c.email)
	formValues.Set("password", c.password)
	formDataStr := formValues.Encode()
	formDataBytes := []byte(formDataStr)
	formBytesReader := bytes.NewReader(formDataBytes)
	req, _ := http.NewRequest(http.MethodPost, httpUrl, formBytesReader)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return 0, "", t, err
	}
	defer res.Body.Close()
	token, _ := io.ReadAll(res.Body)
	err = json.Unmarshal(token, &t)
	if err != nil {
		return 0, string(token), t, err
	}
	return res.StatusCode, string(token), t, err
}
