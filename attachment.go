package bugclose

import (
	"bytes"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"os"
)

/**
 * @Copyright (C) 2022

 * @Author: baoshuangh

 * @Date: 2022/11/12
**/

type UploadAttachmentRequest struct {
	Token      string `json:"token"`
	ProjectId  string `json:"projectId"`
	Attachment string `json:"attachment"`
}

type UploadAttachmentResponse struct {
	Root struct {
		CreatedIP        string `json:"createdIP"`
		Id               int    `json:"id"`
		OriginalFileName string `json:"originalFileName"`
		ProjectId        int    `json:"projectId"`
		UploadTime       int64  `json:"uploadTime"`
		Url              string `json:"url"`
		UserId           int    `json:"userId"`
	} `json:"root"`
	Success bool `json:"success"`
}

type AttachmentService struct {
	client *Client
}

func (a *AttachmentService) DownloadAttachment(url string) (string, []byte, error) {
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	req.Header.Set("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	data, _ := io.ReadAll(res.Body)
	if err != nil {
		return string(data), data, err
	}
	return "", data, nil
}

func (a *AttachmentService) UploadAttachment(r *UploadAttachmentRequest, filePath string) (string, UploadAttachmentResponse, error) {
	var ars UploadAttachmentResponse
	endPoint := "image/upload"
	httpUrl := a.client.baseURL + endPoint
	file, err := os.Open(filePath)
	if err != nil {
		return "", ars, err
	}
	defer file.Close()
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filePath)
	if err != nil {
		return "", ars, err
	}
	_, err = io.Copy(part, file)
	marshal, err := json.Marshal(&r)
	if err != nil {
		return "", ars, err
	}
	var m map[string]string
	_ = json.Unmarshal(marshal, &m)
	for k, v := range m {
		if v != "" {
			_ = writer.WriteField(k, v)
		}
	}
	err = writer.Close()
	if err != nil {
		return "", ars, err
	}
	req, _ := http.NewRequest(http.MethodPost, httpUrl, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := http.DefaultClient.Do(req)
	data, _ := io.ReadAll(res.Body)
	if err != nil {
		return "", ars, err
	} else {
		err := json.Unmarshal(data, &ars)
		if err != nil {
			return string(data), ars, err
		}
	}
	return string(data), ars, nil
}
